# Style Switcher Demo

Demonstrates the capabilities and allows a quick manual testing of the [Style Switcher module](https://www.drupal.org/project/styleswitcher).

## Installation

Install as normally.
Refer to [drupal.org documentation](https://www.drupal.org/docs/getting-started/installing-drupal).
Select "Style Switcher Demo" profile.

### Installation with [Lando](https://lando.dev/)

```shell
mkcd ~/sites/styleswitcher_demo_11
wget -q -O .lando.yml https://git.drupalcode.org/sandbox/pingwin4eg-2547007/-/raw/11.0.x/.lando.yml.dist
lando start
```

## Configuration

The profile comes with a default configuration, which can be changed after the installation.

There is a `css` directory in the profile root containing files which can be used as Style Switcher custom styles.
Example path for a custom style is `profiles/contrib/styleswitcher_demo/css/styleswitcher-custom-terminal.css`.

## Auto-testing and fixing Style Switcher in Lando
### Validate code

```shell
lando parallel-lint
lando phpcs
lando phpstan
lando stylelint
lando eslint
```

### Fix some validation errors

```shell
lando phpcbf
lando eslint-fix
```

### Run tests

```shell
lando phpunit [--filter SomeTest]
lando nightwatch
```
